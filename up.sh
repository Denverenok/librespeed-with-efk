#!/bin/bash
set -e

docker-compose -f efk.compose.yml -p finalefk up -d

docker-compose -f app.compose.yml -p finalapp up -d

docker ps


